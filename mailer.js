const mailer = require('nodemailer')

module.exports = function (config) {
  var transporter = mailer.createTransport({
    service: 'gmail',
    auth: {
      user: config.MAIL_SENDER,
      pass: config.MAIL_PASSWORD
    }
  })

  return function (path, to, onSended) {
    let mailOptions = {
      from: `Samsung Galaxy S10 <${config.MAIL_SENDER}>`,
      to: to.email,
      subject: 'Experiencia Samsung Slow Motion',
      html: `
        <h1> Hola ${to.name} </h1>
        <p>Adjunto tenés tu video Super Slow Motion del nuevo Samsung Galaxy S10 para que puedas compartir con tus amigos y en redes sociales.</p>
        <h2>Que lo disfrutes!</h2>
        <img src='cid:assets/logo.png'/>
        `,
      attachments: [
        /*
        {
          filename: 'logo.png',
          path: 'assets/logo.png',
          cid: 'assets/logo.png'
        },
        */
        {
          filename: 'SamsungSlowMo.mp4',
          path: path
        }
      ]
    }
    transporter.sendMail(mailOptions, onSended)
  }
}
