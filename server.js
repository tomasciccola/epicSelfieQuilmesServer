const http = require('http')
const querystring = require('querystring')
const path = require('path')
const fs = require('fs')
const exec = require('child_process').exec

const VIDS_ROOT         = path.resolve('data/vids')
const NEW_VIDS_PATH     = path.resolve(VIDS_ROOT, 'pulled')
const EDITED_VIDS_PATH  = path.resolve(VIDS_ROOT, 'edited')
const SENDED_VIDS_PATH  = path.resolve(VIDS_ROOT, 'sended')

const mailConfig = require('./config.json')
const adb = require('./adb.js')
const send = require('./mailer.js')(mailConfig)

const SERIAL_PORT = "/dev/ttyACM0"
const serial = require('serialport')(SERIAL_PORT, {baudRate:115200, autoOpen:true})

const db = require('./db.json')

serial.on('data', d => {
  console.log('serial del arduino', d.toString()) 
})

function onRequest(req,res){
  console.log(req.method, req.url)

  let buf = Buffer.allocUnsafe(1)

  switch(req.url){
  case '/new_video':
    parsePost(req)
  break
  case '/start_recording':
    console.log('send start to arduino!')
    serial.write("3")
  break
  case '/reset':
    console.log('send reset to arduino!')
    serial.write("2")
  }
  res.end(null)
}

async function parsePost(req){
  let raw = ''
  let form = {}
  req.on('data', (chunk) => raw += chunk)
  req.on('end', ()=>{
    querystring.decode(raw)
    form = JSON.parse(raw)
    const user = { nombre: form.name, email: form.email }
    db.push(user)
    fs.writeFileSync('db.json', JSON.stringify(db, null, 4))
    console.log('saved to db' , user)
    getVideo(form)
  })
}

function getVideo(form){
  let androidVidpath = form.img.replace(/(^\w+:|^)\/\//, '')
  console.log('copiando video...')
  adb.get(androidVidpath, NEW_VIDS_PATH, (err)=>{
    if(err) return console.log('error agarrando video del celu', err)
    console.log('video copiado')
    let newPath = path.resolve(NEW_VIDS_PATH, path.basename(androidVidpath))
    form.newPath = newPath 
    editVideo(form)
  })
}

function editVideo(form){
  //TODO: por ahora solo copio de pulled a edited
  console.log('editando video...')
  var editedPath = path.resolve(EDITED_VIDS_PATH, path.basename(form.newPath))
  const CMD = `./edit ${form.newPath} ${editedPath}`
  exec(CMD, (error, stdout, stderr) =>{
    if(error) return console.log('error editando video!', err)
    console.log('video editado!')
    fs.unlinkSync(form.newPath)
    form.editedPath = editedPath
    sendVideo(form)
  })

}

function sendVideo(form){
  console.log('enviando mail...')
  send(form.editedPath, {email:form.email, name: form.name}, (err,data) =>{
    if(err) return console.log('error mandando el mail!', err)
    let sendedPath = path.resolve(SENDED_VIDS_PATH, path.basename(form.editedPath))
    console.log('mail enviado exitosamente!')
    console.log(data)
    fs.copyFile(form.editedPath, sendedPath, (err)=>{
      if(err) return console.log('error copiando mail enviado', err)
      console.log('video archivado')
      fs.unlinkSync(form.editedPath)
    })
  })
}

// MAIN
const server = http.createServer(onRequest)

server.listen(8080, ()=>{
  console.log('server arriba')
})
