const spawn = require('child_process').spawn
const fs = require('fs')
const out = fs.openSync('log/adb.log', 'a')
const err = fs.openSync('log/adbErr.log', 'a')

module.exports.ls = function (src) {
  return adb('ls', src)
}

module.exports.get = function (src, dest, onFinished) {
  return adb('pull', src, dest, onFinished)
}

function adb (command, src, dest, cb) {
  let args = []
  let opts = {}

  if (!command) return console.error('adb needs a command arg')
  args.push(command)
  if (src) args.push(src)
  if (dest) args.push(dest)

  if (command === 'pull') {
    opts = { detached: true, stdio: ['ignore', out, err] }
  }

  let ps = spawn('adb', args, opts)

  if (cb) {
    ps.on('exit', function (s) {
      if (s === 0) {
        return cb(null)
      } else {
        return cb(s)
      }
    })

    ps.on('error', function (err) {
      return cb(err)
    })
  }

  // stream error checking
  if (command === 'ls') ps.stderr.pipe(process.stderr)

  return ps
}

